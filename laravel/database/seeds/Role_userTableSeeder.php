<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class Role_userTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// Truncate the user table to clear all users and reset the ID sequence
		DB::table('role_user')->truncate();

		\App\Role_user::create(array(
            'user_id'	=> '1',
            'role_id'	=> '1'
        ));
	}

}
