<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class RoleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// Truncate the user table to clear all users and reset the ID sequence
		DB::table('roles')->truncate();

		\App\Role::create(array(
            'name'			=> 'admin',
            'display_name'	=> 'Administrador',
            'description'	=> 'Administrador del sistema'
        ));

		\App\Role::create(array(
				'name'			=> 'cliente',
				'display_name'	=> 'Cliente',
				'description'	=> 'Accesos al area de Clientes'
		));

		\App\Role::create(array(
				'name'			=> 'usuario',
				'display_name'	=> 'Usuario',
				'description'	=> 'Usuario'
		));

	}

}
