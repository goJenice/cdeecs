<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		// Truncate the user table to clear all users and reset the ID sequence
		DB::table('users')->truncate();

		\App\User::create(array(
            'name'     => 'admin',
            'email'    => 'programacion2@e-consulting.com.mx',
            'password' => \Hash::make('U389IssIeAS*')
        ));
	}

}