@extends('app')

@section('content')
<div class="container inicio">

    @include('common.errors')

    {!! Form::open(['route' => 'memes.store']) !!}

        @include('memes.fields')

    {!! Form::close() !!}
</div>
@endsection
