@extends('app')

@section('content')

    <div class="container inicio">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Memes</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('memes.create') !!}">Agregar</a>
        </div>

        <div class="row">
            @if($memes->isEmpty())
                <div class="well text-center">No Memes found.</div>
            @else
                <table class="table">
                    <thead>
                        {{--<th>Identificador</th>--}}
                        <th>Nombre</th>
                        <th>Descripcion</th>
                        <th>Imagen</th>
                        <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($memes as $meme)
                        <tr>
                            {{--<td>{!! $meme->id !!}</td>--}}
                            <td>{!! $meme->nombre !!}</td>
					        <td>{!! $meme->descripcion !!}</td>
                            <td><a onclick="showImagen(this)">
                                    <img src="{!! $meme->imagen !!}" alt=" " class="minipro">
                                </a>
                            </td>
                            <td>
                                <a href="{!! route('memes.edit', [$meme->id]) !!}">
                                    <i title="Editar" class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('memes.delete', [$meme->id]) !!}"
                                   onclick="return confirm('Seguro de borrar el Meme?')">
                                    <i title="Borrar" class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            <div class="row" align="center">
                <?php echo $memes->render(); ?>
            </div>
            @endif
        </div>
    </div>
@endsection