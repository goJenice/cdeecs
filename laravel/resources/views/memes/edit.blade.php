@extends('app')

@section('content')
<div class="container inicio">

    @include('common.errors')

    {!! Form::model($meme, ['route' => ['memes.update', $meme->id], 'method' => 'patch']) !!}

        @include('memes.fields')

    {!! Form::close() !!}
</div>
@endsection
