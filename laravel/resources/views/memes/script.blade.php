<script>
    var memes = null;

    @if(isset($meme))
        meme = {!! $meme !!};
    @endif
    $(document).ready(function(){
        $(document).on('ready',function(){
            if(meme != null){
                //slider
                $(".imagen").attr("src", meme.imagen);
                $(".imagen").val(meme.imagen);

            }
        });
        //Recorte
        $("#imagen").PictureCut({
            InputOfImageDirectory       : "imagen",
            PluginFolderOnServer        : "/js/piccut/",
            FolderOnServer              : "/img/uploads/memes/",
            EnableCrop                  : true,
            CropWindowStyle             : "Bootstrap",
            EnableButton                : true,
            CropOrientation             : false,
            CropModes : {
                opcion:false,
                letterbox:false,
                widescreen:false,
                free:true,
                slider:true
            },
            EnableMaximumSize           : true,
            MaximumSize                 : 2048
        });
    });
</script>
