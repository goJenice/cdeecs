<div class = "row">
    <div class = "col-xs-6 col-md-3">
        <h1 class = "page-header">Agregar Meme</h1>
    </div>
</div>

<!--- Nombre Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control']) !!}
</div>

<!--- Descripcion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('descripcion', 'Descripcion:') !!}
    {!! Form::text('descripcion', null, ['class' => 'form-control']) !!}
</div>

<!--- Img Field --->
<div class="form-group col-sm-6 col-lg-4" align="center">
    {!! Form::label('imagen', 'Meme (1200*600) Si no requiere cortar la imagen solo dar click en Finalizar:') !!}
    <div id="imagen"></div>
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">

    {!! Form::button('Guardar <i class = "glyphicon glyphicon-floppy-save"></i>',
    ['class' => 'btn btn-success', 'type'=>'submit']) !!}
    <a class = "btn btn-danger" href = "{!! route('memes.index') !!}">
        Cancelar
        <i class = "glyphicon glyphicon-floppy-remove"></i>
    </a>
</div>

@include('memes.script')