@extends('app')
@section('content')
	<div class="container inicio">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<div class="panel panel-default">
					<div class="panel-heading">Administración</div>
					<div class="panel-body">
						<h1>Bienvenido a el Área de Administración</h1>
						<div class="row" align="center">
							<button onclick="location.href='{{ url('/memes') }}'" class = "botonmenu" style="margin: 1px">
								MEMES DE INICIO
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection