<?php

namespace App\Libraries\Repositories;

use App\Models\Meme;
use Illuminate\Support\Facades\Schema;

class MemeRepository
{

	/**
	 * Returns all Memes
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all(){
		return Meme::all();
	}
	public function allslider(){
		return Meme::
		select('memes.*')
			->orderBy('memes.created_at', 'dec')
			->paginate(4);
	}
	public function allgal(){
		return Meme::
		select('memes.*')
			->orderBy('memes.created_at', 'dec')
			->paginate(9);
	}

	public function search($input)
    {
        $query = Meme::query();

        $columns = Schema::getColumnListing('memes');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Meme into database
	 *
	 * @param array $input
	 *
	 * @return Meme
	 */
	public function store($input)
	{
		return Meme::create($input);
	}

	/**
	 * Find Meme by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Meme
	 */
	public function findMemeById($id)
	{
		return Meme::find($id);
	}

	/**
	 * Updates Meme into database
	 *
	 * @param Meme $meme
	 * @param array $input
	 *
	 * @return Meme
	 */
	public function update($meme, $input)
	{
		$meme->fill($input);
		$meme->save();

		return $meme;
	}
}