<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use Mitul\Controller\AppBaseController;
use Mitul\Generator\Utils\ResponseManager;
use App\Models\Meme;
use Illuminate\Http\Request;
use App\Libraries\Repositories\MemeRepository;
use Response;
use Schema;

class MemeAPIController extends AppBaseController
{

	/** @var  MemeRepository */
	private $memeRepository;

	function __construct(MemeRepository $memeRepo)
	{
		$this->memeRepository = $memeRepo;
	}

	/**
	 * Display a listing of the Meme.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->memeRepository->search($input);

		$memes = $result[0];

		return Response::json(ResponseManager::makeResult($memes->toArray(), "Memes retrieved successfully."));
	}

	public function search($input)
    {
        $query = Meme::query();

        $columns = Schema::getColumnListing('$TABLE_NAME$');
        $attributes = array();

        foreach($columns as $attribute)
        {
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
            }
        }

        return $query->get();
    }

	/**
	 * Show the form for creating a new Meme.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created Meme in storage.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Meme::$rules) > 0)
            $this->validateRequest($request, Meme::$rules);

        $input = $request->all();

		$meme = $this->memeRepository->store($input);

		return Response::json(ResponseManager::makeResult($meme->toArray(), "Meme saved successfully."));
	}

	/**
	 * Display the specified Meme.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$meme = $this->memeRepository->findMemeById($id);

		if(empty($meme))
			$this->throwRecordNotFoundException("Meme not found", ERROR_CODE_RECORD_NOT_FOUND);

		return Response::json(ResponseManager::makeResult($meme->toArray(), "Meme retrieved successfully."));
	}

	/**
	 * Show the form for editing the specified Meme.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified Meme in storage.
	 *
	 * @param  int    $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$meme = $this->memeRepository->findMemeById($id);

		if(empty($meme))
			$this->throwRecordNotFoundException("Meme not found", ERROR_CODE_RECORD_NOT_FOUND);

		$input = $request->all();

		$meme = $this->memeRepository->update($meme, $input);

		return Response::json(ResponseManager::makeResult($meme->toArray(), "Meme updated successfully."));
	}

	/**
	 * Remove the specified Meme from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$meme = $this->memeRepository->findMemeById($id);

		if(empty($meme))
			$this->throwRecordNotFoundException("Meme not found", ERROR_CODE_RECORD_NOT_FOUND);

		$meme->delete();

		return Response::json(ResponseManager::makeResult($id, "Meme deleted successfully."));
	}
}
