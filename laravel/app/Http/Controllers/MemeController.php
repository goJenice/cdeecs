<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateMemeRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\MemeRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class MemeController extends AppBaseController
{

	/** @var  MemeRepository */
	private $memeRepository;

	function __construct(MemeRepository $memeRepo)
	{
		$this->memeRepository = $memeRepo;
	}

	/**
	 * Display a listing of the Meme.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->memeRepository->search($input);

		$memes = $result[0];

		$attributes = $result[1];
		$memes = $this->memeRepository->allgal();

		return view('memes.index')
		    ->with('memes', $memes)
		    /*->with('attributes', $attributes);*/;
	}

	/**
	 * Show the form for creating a new Meme.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('memes.create');
	}

	/**
	 * Store a newly created Meme in storage.
	 *
	 * @param CreateMemeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateMemeRequest $request)
	{
        $input = $request->all();

		/*dd($input);*/

		$meme = $this->memeRepository->store($input);

		Flash::message('Meme Guardado.');

		return redirect(route('memes.index'));
	}

	/**
	 * Display the specified Meme.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$meme = $this->memeRepository->findMemeById($id);

		if(empty($meme))
		{
			Flash::error('Meme no encontrado');
			return redirect(route('memes.index'));
		}

		return view('memes.show')->with('meme', $meme);
	}

	/**
	 * Show the form for editing the specified Meme.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$meme = $this->memeRepository->findMemeById($id);

		if(empty($meme))
		{
			Flash::error('Meme no encontrado');
			return redirect(route('memes.index'));
		}

		return view('memes.edit')->with('meme', $meme);
	}

	/**
	 * Update the specified Meme in storage.
	 *
	 * @param  int    $id
	 * @param CreateMemeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateMemeRequest $request)
	{
		$meme = $this->memeRepository->findMemeById($id);

		if(empty($meme))
		{
			Flash::error('Meme no encontrado');
			return redirect(route('memes.index'));
		}

		$meme = $this->memeRepository->update($meme, $request->all());

		Flash::message('Meme actualizado.');

		return redirect(route('memes.index'));
	}

	/**
	 * Remove the specified Meme from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id){
		$meme = $this->memeRepository->findMemeById($id);

		if(empty($meme)){
			Flash::error('Meme no encontrado');
			return redirect(route('memes.index'));
		}

		$meme->delete();

		Flash::message('Meme borrado.');

		return redirect(route('memes.index'));
	}

}
