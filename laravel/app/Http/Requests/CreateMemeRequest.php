<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Meme;

class CreateMemeRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return Meme::$rules;
	}
	public function messages()
	{
		return [

				'nombre.required' => 'El nombre es requerido',
				'imagen.required' => 'La imagen es requerida',

		];
	}

}
