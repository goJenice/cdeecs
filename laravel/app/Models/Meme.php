<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Meme extends Model
{
    use SoftDeletes;

	public $table = "memes";

	public $primaryKey = "id";
    
	protected $dates = ['deleted_at'];

	public $timestamps = true;

	public $fillable = [
	    "nombre",
		"descripcion",
		"imagen"
	];

	public static $rules = [
	    "nombre" => "required",
	    "imagen" => "required"
	];

}
