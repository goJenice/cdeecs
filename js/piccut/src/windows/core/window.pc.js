$(function() {
    var $t = function(selector) {
        return $("#JtuyoshiCrop").find(selector)
    };
    var SelectMudar_Orientacao = function() {
        var CW,CH,CX,CY;
        if ($t("#SelectProporcao").val() == "box") {
            CH = (($t("#Principal").height() / 100) * 80);
            CW = (CH * 4) / 4;
            if (CW > $t("#Principal").width()) {
                CW = (($t("#Principal").width() / 100) * 80);
                CH = (CW / 4) * 4
            }
            CX = (($t("#Principal").width() - CW) / 2);
            CY = (($t("#Principal").height() - CH) / 2);
            $t("#SelecaoRecorte").stop().animate({
                "left": CX,
                "top": CY,
                "width": CW,
                "height": CH
            }, 300);
            $t("#SelecaoRecorte").resizable("destroy").resizable({
                containment: "parent",
                aspectRatio: 4 / 4,
                minWidth: 100,
                minHeight: 100
            })
        }
        else if ($t("#SelectProporcao").val() == "bannerDeli") {
            CW = 970;
            CH = 370;  //Cambio
            CX = (($t("#Principal").width() - CW) / 2);
            CY = (($t("#Principal").height() - CH) / 2);

            $t("#SelecaoRecorte").stop().animate({
                "left": CX,
                "top": CY,
                "width": CW,
                "height": CH
            }, 300);
            $t("#SelecaoRecorte").resizable("destroy").resizable({
                containment: "parent",
                aspectRatio: 970 / 370,  //Cambio
                minWidth: 970,
                minHeight: 370
            })
        }else if ($t("#SelectProporcao").val() == "slider") {
            if ($t("#Principal").width() < $t("#Principal").height()) {
                CW = 600;
                CH = 300;
                CX = (($t("#Principal").width() - CW) / 2);
                CY = (($t("#Principal").height() - CH) / 2);
            } else {

                CH = 300;
                CW = 600;

                CX = (($t("#Principal").width() - CW) / 2);
                CY = (($t("#Principal").height() - CH) / 2);
            };
            $t("#SelecaoRecorte").stop().animate({
                "left":     CX,
                "top":      CY,
                "width":    CW,
                "height":   CH
            }, 300);
            $t("#SelecaoRecorte").resizable("destroy").resizable({
                containment: "parent",
                aspectRatio: 2 / 1,
                minWidth:   600,
                minHeight:  300,
                maxWidth:   600,
                maxHeight:  300
            })
        }else if ($t("#SelectProporcao").val() == "picProducto") {
            CW = 50;
            CH = 50;  //Cambio
            CX = (($t("#Principal").width() - CW) / 2);
            //CX = 294;
            CY = (($t("#Principal").height() - CH) / 2);
            //CY = 363;

            $t("#SelecaoRecorte").stop().animate({
                "left": CX,
                "top": CY,
                "width": CW,
                "height": CH
            }, 300);
            $t("#SelecaoRecorte").resizable("destroy").resizable({
                containment: "parent",
                aspectRatio: 440 / 342,  //Cambio
                minWidth: 20, //cambio
                minHeight: 20 //cambio
            })
        }
        else if ($t("#SelectProporcao").val() == "picCategoria") { //LOGO //betoflakes
            console.log("PicCategoria");
            CW = 230;
            CH = 203;  //Cambio
            CX = (($t("#Principal").width() - CW) / 2);
            CY = (($t("#Principal").height() - CH) / 2);

            $t("#SelecaoRecorte").stop().animate({
                "left": CX,
                "top": CY,
                "width": CW,
                "height": CH
            }, 300);
            $t("#SelecaoRecorte").resizable("destroy").resizable({
                containment: "parent",
                aspectRatio: 230 / 230,
                minWidth: 150,
                minHeight: 150
            })
        }

    };
    $t("#SelectOrientacao, #SelectProporcao").change(function() {
        SelectMudar_Orientacao()
    });
    $t("#SelectProporcao").trigger("change");
});