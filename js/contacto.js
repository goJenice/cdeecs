//Regex para validar email
var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

function validarNombre(){
	if($("#nombre").val().trim() == "")
		return false;
	return true;
}
function validarTelefono() {
	var numeroTelefono = $("#telefono").val().trim();
	var expresionRegular1 = /^([0-9]+){10}$/;//<--- con esto vamos a validar el numero
	// /^\d{3}-\d{7}$/ tres digitos mas un gion mas siete digitos
	var expresionRegular2 = /\s/;//<--- con esto vamos a validar que no tenga espacios en blanco

	if (numeroTelefono == "") {
		alert('El campo de Telefono es necesario');
		return false;
	} else if (expresionRegular2.test(numeroTelefono)) {
		alert('Existen espacios en blanco');
		return false;
	} else if (!expresionRegular1.test(numeroTelefono) || numeroTelefono.length >= 11 ) {
		alert('Son nesesarios 10 digitos numericos');
		return false;
	}else {
		return true;
	}
}
function validarEmail() {
	if ($("#email").val().trim() == ""){
		alert("El campo de Correo Electrónico es necesario")
		return false;
	} else if(!regex.test($("#email").val())) {
		alert("Ingrese un campo de Correo Electrónico Valido");
		return false;
	}
	return true;
}
function validarMensaje(){
	if($("#mensaje").val().trim() == "")
		return false;
	return true;
}
function enviar(lang){
	if(!validarNombre()){
		if(lang == "es") alert("El campo de Nombre es necesario");
		if(lang == "en") alert("Name is required.");
		return;
	}
	if(!validarTelefono()){
		return;
	}
	if(!validarEmail()){
		return;
	}
	if($("#code").val().trim() == ""){
		if(lang == "es") alert("Ingresa los caracteres que ves en la imagen.");
		if(lang == "en") alert("Enter the characters in the picture");
		return;
	}
	if(!validarMensaje()){
		if(lang == "es"){
			if(!confirm("¿Deseas enviar este mensaje en blanco?"))
				return; //Si no desea enviar el mensaje en blanco, sale de la función
		}
		if(lang == "en"){
			if(!confirm("Do you really want to send a blank message?"))
				return; //Si no desea enviar el mensaje en blanco, sale de la función
		}
	}
	$.ajax({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		},
		type: 'POST',
		url    : '/web/enviar-correo',
		data: {
			nombre:		$("#nombre").val().trim(),
			telefono:	$("#telefono").val().trim(),
			email:		$("#email").val().trim(),
			asunto:		$("#asunto").val().trim(),
			mensaje:	$("#mensaje").val().trim(),
			code:		$("#code").val().trim(),
			lang: 		lang
		},
		success: function (mensaje){
			if(lang == "es"){
				switch(parseInt(mensaje)){
					case -1: //Código incorrecto
						alert("Código erróneo. Intenta nuevamente.");
						break;
					case 0: //Problema al enviar el correo
						alert("Hubo un problema al enviar el correo. Intenta de nuevo más tarde.");
						break;
					case 1: //Envío exitoso
						alert("Gracias por comunicarte con nosotros. En breve nos pondremos en contacto contigo.");
						emptyfields();
						break;
					default: //Hay que revisar qué pasó
						console.log(mensaje);
						break;
				}
			}else{
				switch(parseInt(mensaje)){
					case -1: //Código incorrecto
						alert("Wrong code. Please try again.");
						break;
					case 0: //Problema al enviar el correo
						alert("There was a problem sending the mail. Please try again later.");
						break;
					case 1: //Envío exitoso
						alert("Thanks for contacting us. We will contact you soon.");
						emptyfields();
						break;
					default: //Hay que revisar qué pasó
						console.log(mensaje);
						break;
				}
			}
		}
	})
}

function emptyfields(){
	$("#nombre, #telefono, #email, #mensaje, #asunto, #code").val("");
}

$(document).ready(function(){
	console.log("Listo!");
});